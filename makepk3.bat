@echo off
REM autobuild for NIST

REM check if the directories exist

goto check

:missing
echo Some data is missing! :(
pause
exit

:check
if not exist .\acc\acc.exe goto missing
if not exist .\7za\7za.exe goto missing
if not exist .\pk3data\ goto missing

REM compile ACS

cd acc
acc ..\pk3data\nist ..\pk3data\acs\nist
cd ..

REM end compile ACS

REM packaging

for /f %%i in ('hg identify -n') do set hgrev=%%i
cd 7za 
7za a -t7z ..\NIST-r%hgrev%.pk7 ..\pk3data\*
cd ..

REM end packaging

REM remove ACS object file so next time this is ran there won't be 2 object files

cd pk3data\acs
del *.o
pause